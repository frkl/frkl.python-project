# -*- coding: utf-8 -*-
import asyncclick as click


# from frkl.args.arg import RecordArg, from_python_type
# from frkl.args.hive import ArgHive


click.anyio_backend = "asyncio"

# arg_hive = ArgHive()


# class CookiecutterArguments(object):
#     def __init__(
#         self,
#         cookiecutter_config: Mapping[str, Any],
#         template_argument_schema: Mapping[str, Any] = None,
#     ):
#
#         self._cookiecutter_config: Mapping[str, Any] = cookiecutter_config
#         self._template_argument_schema: Mapping[str, Any] = {}
#
#         for k, v in self._cookiecutter_config.items():
#             if k.startswith("_"):
#                 continue
#
#             if isinstance(v, str):
#                 self._template_argument_schema[k] = {"default": v, "type": "string"}
#             elif isinstance(v, collections.abc.Mapping):
#                 self._template_argument_schema[k] = {"default": v, "type": "dict"}
#
#             elif isinstance(v, collections.abc.Iterable):
#                 v = list(v)
#                 first_value = v[0]
#                 value_type = from_python_type(first_value.__class__)
#                 if len(value_type) == 1:
#                     value_type_name = value_type[0]
#                 else:
#                     value_type_name = "any"
#                 self._template_argument_schema[k] = {
#                     "default": first_value,
#                     "type": value_type_name,
#                     "allowed": v,
#                 }
#             else:
#                 value_type = from_python_type(v)
#                 if len(value_type) != 1:
#                     raise ValueError(f"Can't determine value type: {v}")
#                 value_type_name = value_type[0]
#                 self._template_argument_schema[k] = {
#                     "default": v,
#                     "type": value_type_name,
#                 }
#
#         self._args: RecordArg = arg_hive.create_record_arg(
#             self._template_argument_schema
#         )
#
#     @property
#     def args(self):
#         return self._args


@click.command()
@click.pass_context
async def cli(ctx):

    print("Hello")

    # project_meta = ProjectMetadata(main_module="frkl.common")
    #
    # from prompt_toolkit import prompt
    # from prompt_toolkit import PromptSession
    # from prompt_toolkit.patch_stdout import patch_stdout
    #
    # template_git_url = "https://gitlab.com/frkl/frkl.python-project.git"
    #
    # result = await ensure_repo_cloned(template_git_url)
    #
    # cookiecutter_config_path = os.path.join(result, "cookiecutter.json")
    #
    # cookiecutter_config = json.loads(Path(cookiecutter_config_path).read_text())
    # print(cookiecutter_config)
    #
    # ca = CookiecutterArguments(cookiecutter_config=cookiecutter_config)
    #
    # print(ca.args)
    #
    # session = PromptSession()
    # while True:
    #
    #     with patch_stdout():
    #         result = await session.prompt_async(
    #             "Say something: ", completer=LicenseNameCompleter()
    #         )
    #     print("You said: %s" % result)
    #
    # parser = create_parser()
    # args = parser.parse_args(
    #     [
    #         "--from=mixed",
    #         "--with-authors",
    #         "--with-urls",
    #         "--with-description",
    #         "--with-license-file",
    #         "--format",
    #         "json",
    #     ]
    # )
    # output_string = create_output_string(args)
    # dependency_list = json.loads(output_string)
    #
    # for d in dependency_list:
    #     print(d.keys())
    #
    # output_string = create_output_string(args)
    # print(output_string)
    #
    # output_file = args.output_file
    # save_if_needs(output_file, output_string)
    #
    # warn_string = create_warn_string(args)
    # if warn_string:
    #     print(warn_string, file=sys.stderr)


if __name__ == "__main__":
    cli()
