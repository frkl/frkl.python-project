# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs


frkl_python_project_app_dirs = AppDirs("frkl_python_project", "frkl")

if not hasattr(sys, "frozen"):
    FRKL_PYTHON_PROJECT_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `frkl_python_project` module."""
else:
    FRKL_PYTHON_PROJECT_MODULE_BASE_FOLDER = os.path.join(
        sys._MEIPASS, "frkl_python_project"  # type: ignore
    )
    """Marker to indicate the base folder for the `frkl_python_project` module."""

FRKL_PYTHON_PROJECT_RESOURCES_FOLDER = os.path.join(
    FRKL_PYTHON_PROJECT_MODULE_BASE_FOLDER, "resources"
)

SPDX_LICENSE_JSON_URL = "https://spdx.org/licenses/licenses.json"
