# -*- coding: utf-8 -*-
from typing import Iterable

from frkl.python_project.licenses.spdx import Licenses
from prompt_toolkit.completion import CompleteEvent, Completer, Completion
from prompt_toolkit.document import Document


class LicenseNameCompleter(Completer):
    def __init__(self):

        self._licenses: Licenses = Licenses()

    def get_completions(
        self, document: Document, complete_event: CompleteEvent
    ) -> Iterable[Completion]:

        for lic in self._licenses.find_license(
            document.text_before_cursor, ignore_case=True
        ):
            yield Completion(text=lic.name, start_position=-len(document.text))
