# -*- coding: utf-8 -*-
import json
from dataclasses import dataclass
from typing import Any, Iterable, List, Mapping, Optional

from frkl.common.downloads import FILE_CONTENT_TYPE
from frkl.common.downloads.cache import download_cached_file
from frkl.python_project.defaults import SPDX_LICENSE_JSON_URL


@dataclass
class License(object):

    # @classmethod
    # def create(cls, **license_data):
    #
    #     kwargs = {}
    #     extra_data = {}
    #     for k, v in license_data.items():
    #
    #         if k in cls.__dataclass_fields__.keys():
    #             kwargs[k] = v
    #         else:
    #             print("=================")
    #             for t in list(cls.__dataclass_fields__.keys()):
    #                 print(t)
    #             print("----")
    #             print(k)
    #
    #     # kwargs["extra_data"] = extra_data
    #
    #     print(kwargs.keys())
    #
    #     import pp
    #     pp(license_data)
    #
    #
    #     return License(**kwargs)

    reference: str
    isDeprecatedLicenseId: bool
    detailsUrl: str
    referenceNumber: str
    name: str
    licenseId: str
    seeAlso: Iterable[str]
    licenseListVersion: str
    isOsiApproved: Optional[bool] = None
    isFsfLibre: Optional[bool] = None

    def __hash__(self):
        return hash(self.licenseId)

    def get_details(self, update: bool = False) -> Mapping[str, Any]:

        content = download_cached_file(
            self.detailsUrl,
            content_type=FILE_CONTENT_TYPE.text,
            return_content=True,
            update=update,
        )
        return json.loads(content)

    def get_license_detail(self, detail_name: str):

        return self.get_details()[detail_name]

    @property
    def licenseText(self) -> str:

        return self.get_details()["licenseText"]

    @property
    def standardLicenseTemplate(self):

        return self.get_details()["standardLicenseTemplate"]

    @property
    def alias(self) -> str:

        return self.reference[2:-5]

    def __repr__(self):

        return f"License(name={self.name})"

    def __str__(self):
        return self.name


def get_license_list(update: bool = False) -> List[License]:

    content = download_cached_file(
        SPDX_LICENSE_JSON_URL,
        content_type=FILE_CONTENT_TYPE.text,
        return_content=True,
        update=update,
    )
    license_data = json.loads(content)

    result = []
    for ld in license_data["licenses"]:
        license = License(licenseListVersion=license_data["licenseListVersion"], **ld)
        result.append(license)
    return result


class Licenses(object):
    def __init__(self):

        self._licenses: Iterable[License] = None  # type: ignore

    @property
    def licenses(self) -> Iterable[License]:
        if self._licenses is None:
            self._licenses = get_license_list()
        return self._licenses

    def find_license(
        self, search_string: str, ignore_case: bool = True
    ) -> Iterable[License]:

        for lic in self.licenses:
            if ignore_case:
                if (
                    search_string.lower() in lic.name.lower()
                    or search_string.lower() in lic.alias.lower()
                ):
                    yield lic
                elif search_string in lic.name or search_string in lic.alias:
                    yield lic
