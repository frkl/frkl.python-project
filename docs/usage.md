# Usage

## Getting help

To get information for the `frkl.python-project` command, use the ``--help`` flag:

{{ cli("frkl.python-project", "--help") }}
