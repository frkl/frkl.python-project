#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `frkl_python_project` package."""

import frkl.python_project
import pytest  # noqa


def test_assert():

    assert frkl.python_project.get_version() is not None
