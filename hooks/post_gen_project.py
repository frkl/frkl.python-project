#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import shutil
from pathlib import Path


PROJECT_DIRECTORY = os.path.realpath(os.path.curdir)
SRC_DIRECTORY = os.path.join(PROJECT_DIRECTORY, "src")

module_name = "{{ cookiecutter.project_main_module }}"

if "." in module_name:

    main_module_path = os.path.join(SRC_DIRECTORY, module_name)

    tokens = module_name.split(".")

    current_dir = SRC_DIRECTORY
    for t in tokens[0:-1]:
        current_dir = os.path.join(current_dir, t)
        os.mkdir(current_dir)
        init_file = os.path.join(current_dir, "__init__.py")
        p = Path(init_file)
        p.write_text(
            """# -*- coding: utf-8 -*-
__path__ = __import__("pkgutil").extend_path(__path__, __name__)  # type: ignore
"""
        )

    last_token = tokens[-1]
    new_main_module_path = os.path.join(current_dir, last_token)
    shutil.move(main_module_path, new_main_module_path)

if __name__ == "__main__":

    pass
